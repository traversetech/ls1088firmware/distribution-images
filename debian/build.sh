#!/bin/bash
set -e

# Use this to layer additional elements on top of the base debian image
# (e.g for application-specific appliances)
export EXTRA_ELEMENTS=${EXTRA_ELEMENTS:-}
export IMAGE_BASE_NAME=${IMAGE_BASE_NAME:-debian}
export CI_PIPELINE_IID=${CI_PIPELINE_IID:-localbuild}
BUILD_DATE=$(date +%Y%m%d)
BUILD_REV="${BUILD_DATE}-${CI_PIPELINE_IID}"
# Attempt to install all builddeps if in a fresh (e.g cloud runner) environment
if [ ! -f /usr/bin/tox ]; then
  apt-get install -y tox build-essential libpython3-dev libffi-dev libssl-dev qemu-utils debootstrap kpartx
fi

pushd ../diskimage-builder/
# Need the last upstream tag reference
git fetch -a
#Fixme: add defaults
export DIB_NETWORK_INTERFACE_NAMES=""

export TRAVERSE_DEBIAN_KERNEL_REPO_URL=${TRAVERSE_DEBIAN_KERNEL_REPO_URL:-https://archive.traverse.com.au/pub/traverse/debian-experimental/}
export TRAVERSE_DEBIAN_KERNEL_DIST=${TRAVERSE_DEBIAN_KERNEL_DIST:-lts-5-4}

echo "Building with kernel repo: ${TRAVERSE_DEBIAN_KERNEL_REPO_URL}"
echo "Building with kernel dist: ${TRAVERSE_DEBIAN_KERNEL_DIST}"

#echo "Tox bindep":
#tox -e bindep
#echo "Doing build:"
tox -e venv -- disk-image-create debian debian-systemd vm block-device-efi \
  traverse-kernel-apt traverse-motd defaultrootpw "${EXTRA_ELEMENTS}"

popd
mkdir -p output
mv ../diskimage-builder/image.qcow2 "output/${IMAGE_BASE_NAME}.qcow2"
cd output

echo "SHA256 for uncompressed images:"
sha256sum "${IMAGE_BASE_NAME}.qcow2" | tee "${IMAGE_BASE_NAME}.qcow2.sha256sum"

export APPSTORE_ID=${APPSTORE_ID:-traverse-debian-buster}
export APPSTORE_NAME=${APPSTORE_NAME:-Traverse Debian Buster image}
export APPSTORE_DESCRIPTION=${APPSTORE_DESCRIPTION:-Debian buster with Traverse customizations. Root password is "changethis"}

../../generate-metadata.py "${APPSTORE_ID}" "output/${IMAGE_BASE_NAME}.qcow2" "${APPSTORE_NAME}" \
  "${APPSTORE_DESCRIPTION}" \
  "${BUILD_REV}" "${IMAGE_BASE_NAME}.qcow2" "${IMAGE_BASE_NAME}.qcow2.sha256sum" "metadata.json"
