#!/bin/sh
set -e

IMAGE_NAME="${1}"
remote_ssh() {
  ssh -i /tmp/muvirt_user_key -o 'CheckHostIP no' -o "UserKnownHostsFile /tmp/muvirt_host_key" \
    "root@${MUVIRT_TEST_HOST}" $@
}

remote_scp() {
  scp -i /tmp/muvirt_user_key -o 'CheckHostIP no' -o "UserKnownHostsFile /tmp/muvirt_host_key" $@
}

echo "${MUVIRT_USER_KEY}" >> /tmp/muvirt_user_key
echo "${MUVIRT_TEST_HOST} ${MUVIRT_HOST_KEY}" >> /tmp/muvirt_host_key
#cat /tmp/muvirt_host_key
chmod 0600 /tmp/muvirt_host_key
chmod 0600 /tmp/muvirt_user_key

if [ ! -f "${IMAGE_NAME}" ]; then
  echo "ERROR: ${IMAGE_NAME} is not present - check CI configuration"
  exit 1
fi
ls -la "${IMAGE_NAME}"

remote_scp test/muvirt/citest.sh "root@${MUVIRT_TEST_HOST}:/tmp"
remote_ssh "chmod +x /tmp/citest.sh"

UNIQUE_IMAGE_NAME="citest-image-${CI_PIPELINE_ID}.qcow2"
#Download and start
remote_scp "${IMAGE_NAME}" "root@${MUVIRT_TEST_HOST}:/tmp/${UNIQUE_IMAGE_NAME}"

remote_ssh "TEST_NETWORK=${MUVIRT_TEST_NETWORK} PIPELINE_ID=${CI_PIPELINE_ID} IMAGE_NAME=${UNIQUE_IMAGE_NAME} LOGIN_HOSTNAME=${IMAGE_HOSTNAME} /tmp/citest.sh"
remote_scp "root@${MUVIRT_TEST_HOST}:/tmp/cibootlog" vm_boot_log.txt

JOB_IMAGE_NAME=$(echo "${CI_JOB_NAME}" | awk -F ':' '{print $NF}')
mkdir -p "testlogs/${JOB_IMAGE_NAME}/vm"
mv vm_boot_log.txt "testlogs/${JOB_IMAGE_NAME}/vm"
