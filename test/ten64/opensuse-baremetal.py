#!/usr/bin/env python3

from testutil import BareMetalTestClient
import os
import sys

def configure_guest(testclient):
    testclient.send_simple_command("partprobe {}".format(REMOTE_BLOCK_DEVICE))
    testclient.send_simple_command("mount {}p3 /mnt".format(REMOTE_BLOCK_DEVICE))
    testclient.send_simple_command("rm /mnt/etc/network/interfaces.d/*".format(REMOTE_BLOCK_DEVICE))

    testclient.send_simple_command("echo 'auto {}' >> /mnt/etc/network/interfaces.d/{}".format(WAN_INTERFACE,WAN_INTERFACE))
    testclient.send_simple_command("echo 'iface {} inet dhcp' >> /mnt/etc/network/interfaces.d/{}".format(WAN_INTERFACE,WAN_INTERFACE))
    testclient.send_simple_command("echo 'network: {config: disabled}' > /mnt/etc/cloud/cloud.cfg.d/99-disable-network-config.cfg")
    testclient.send_simple_command("echo -n 'debian:test123' | chroot /mnt 'chpasswd'")
    testclient.send_simple_command("umount /mnt")

    testclient.send_simple_command("sync")

REMOTE_CONSOLE_HOST = os.environ["REMOTE_CONSOLE_HOST"]
REMOTE_CONSOLE_PORT = os.environ["REMOTE_CONSOLE_PORT"]

BMC_REDFISH_BASE = os.environ["BMC_REDFISH_URL"]

REMOTE_BLOCK_DEVICE = os.environ["REMOTE_BLOCK_DEVICE"]

IMAGE_NAME = os.environ["IMAGE_NAME"]

WAN_INTERFACE = os.environ["WAN_INTERFACE"]

TRANSFER_MODE = "ssh"
LOCAL_IMAGE_PATH = None
if ("TRANSFER_MODE" in os.environ):
    TRANSFER_MODE = os.environ["TRANSFER_MODE"]

if (TRANSFER_MODE == "ssh"):
    LOCAL_IMAGE_PATH = os.environ["LOCAL_IMAGE_PATH"]
else:
    IMAGE_DOWNLOAD_URL = "{}{}/{}/image/{}".format(os.environ["DOWNLOAD_SITE_BASE"],
        os.environ["CI_COMMIT_REF_NAME"],
        os.environ["CI_PIPELINE_ID"],
        IMAGE_NAME)
    print("Will download image from {}".format(IMAGE_DOWNLOAD_URL))

BOOTLOG_FILE = None

EXPECTED_ETHERNET_INTERFACES = 10
if ("EXPECTED_ETH_INTFS" in os.environ):
    EXPECTED_ETHERNET_INTERFACES = int(os.environ["EXPECTED_ETH_INTFS"])

if (len(sys.argv) >= 2):
    BOOTLOG_FILE = sys.argv[1]
    print("dmesg will be written to {}".format(BOOTLOG_FILE))

OPENSUSE_DEFAULT_PS1 = "localhost:~ #"

SKIP_DOWNLOAD = False

testclient = BareMetalTestClient(REMOTE_CONSOLE_HOST, REMOTE_CONSOLE_PORT, BMC_REDFISH_BASE, "/dev/nvme0n1", WAN_INTERFACE)
testclient.connect()
if (SKIP_DOWNLOAD == False):
    testclient.make_ready_for_download()
    if (TRANSFER_MODE == "ssh"):
        testclient.do_ssh_image_transfer(LOCAL_IMAGE_PATH, IMAGE_NAME)
    configure_guest(testclient)

testclient.reset_into_default_boot()
testclient.set_target_login_prompt("localhost login:")
testclient.set_target_ps1(OPENSUSE_DEFAULT_PS1)
testclient.client.expect('localhost login:',timeout=180)
bootlog = testclient.client.before
if (BOOTLOG_FILE != None):
    bootlog_output = open(BOOTLOG_FILE,"w")
    print(bootlog, file=bootlog_output)
    bootlog_output.close()
testclient.handle_first_root_login("changethis","3seBRo5t7CfYmwrS")
testclient.set_stty_cols()
testclient.check_free_diskspace()
testclient.check_number_of_interfaces()
#testclient.has_hardware_rtc()
#testclient.has_default_route()
#testclient.do_apt_get_update()
testclient.check_can_reset()

exit(0)
