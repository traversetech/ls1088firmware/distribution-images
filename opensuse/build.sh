#!/bin/bash
set -e

# Use this to layer additional elements on top of the base debian image
# (e.g for application-specific appliances)
export EXTRA_ELEMENTS=${EXTRA_ELEMENTS:-}
export IMAGE_BASE_NAME=${IMAGE_BASE_NAME:-opensuse}
export CI_PIPELINE_IID=${CI_PIPELINE_IID:-localbuild}
BUILD_DATE=$(date +%Y%m%d)
BUILD_REV="${BUILD_DATE}-${CI_PIPELINE_IID}"

# Attempt to install all builddeps if in a fresh (e.g cloud runner) environment
if [ ! -f /usr/bin/tox ]; then
  apt-get install -y tox build-essential libpython3-dev libffi-dev libssl-dev qemu-utils debootstrap kpartx
fi

pushd ../diskimage-builder/
# Need the last upstream tag reference
git fetch -a
tox -e venv -- disk-image-create opensuse vm block-device-efi \
  traverse-motd defaultrootpw opensuse-traverse-addons "${EXTRA_ELEMENTS}"

popd
mkdir output
mv ../diskimage-builder/image.qcow2 "output/${IMAGE_BASE_NAME}.qcow2"
cd output

echo "SHA256 for uncompressed images:"
sha256sum "${IMAGE_BASE_NAME}.qcow2" | tee "${IMAGE_BASE_NAME}.qcow2.sha256sum"

export APPSTORE_ID=${APPSTORE_ID:-traverse-opensuse-151}
export APPSTORE_NAME=${APPSTORE_NAME:-Traverse openSuSE 15.1 image}
export APPSTORE_DESCRIPTION=${APPSTORE_DESCRIPTION:-'openSuSE cloud build rewrapped with EFI and cloud-init support. Root password is "changethis"'}

../../generate-metadata.py "${APPSTORE_ID}" "${IMAGE_BASE_NAME}.qcow2" "${APPSTORE_NAME}" \
  "${APPSTORE_DESCRIPTION}" \
  "${BUILD_REV}" "${IMAGE_BASE_NAME}.qcow2" "${IMAGE_BASE_NAME}.qcow2.sha256sum" "metadata.json"
